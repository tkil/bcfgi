//@ts-check
"use strict"
const { mkdir } = require("fs/promises")
const sharp = require("sharp")
const path = require("path")

const DIR_ROOT = path.resolve(__dirname)
const FILE_NAME_NO_EXTENSION = "background-church"

const FILEPATH_SOURCE_IMAGE = path.join(
  DIR_ROOT,
  "source",
  "background-church-source.jpg"
)
const DIR_OUTPUT = path.join(DIR_ROOT, "output")

const CROP_TOP = 400
const CROP_BOTTOM = 400
const SIZES = [500, 800]
const FORMATS = ["jpg", "webp", "avif"]

const main = async () => {
  await mkdir(DIR_OUTPUT, { recursive: true })

  const sp = sharp(FILEPATH_SOURCE_IMAGE)
  const { height, width } = await sp.metadata()
  const spCropped = sp.extract({
    height: (height ?? 0) - CROP_TOP - CROP_BOTTOM,
    left: 0,
    top: CROP_TOP,
    width: width ?? 0,
  })
  for (const size of SIZES) {
    const spSized = spCropped.resize(size)
    for (const format of FORMATS) {
      await spSized.toFile(
        path.join(DIR_OUTPUT, `${FILE_NAME_NO_EXTENSION}-${size}.${format}`)
      )
    }
  }
}
main()
