import { LitElement, css, html } from "lit"

/**
 * An example element.
 *
 * @csspart button - The button
 */
export class SermonCard extends LitElement {
  static get properties() {
    return {
      /**
       * Copy for the read the docs hint.
       */
      docsHint: { type: String },

      /**
       * The number of times the button has been clicked.
       */
      count: { type: Number },
    }
  }

  constructor() {
    super()
  }

  //   "Water-Wine-Blood, Revelation 14:14-20"

  // Sermon Only
  // March 10, 2024 - Menu

  // Menu
  // Sermon
  // Full Service

  render() {
    return html`
      <div class="sermon-card">
        <div class="sermon-card__inner">
          <div class="sermon-card__info">
            <div class="sermon-card__header">
              <div>Water, Wine, Blood</div>
              <div>3-10-2024</div>
            </div>
            <div class="sermon-card__scriptures">
              Scripture:
              <ul>
                <li>Revelation 14:14-20</li>
                <li>Psalms 16:1-4</li>
              </ul>
            </div>
          </div>
          <ul class="sermon-card__links">
            <li><a href="#">Full Service</a></li>
            <li><a href="#">Sermon Only</a></li>
            <li><a href="#">Menu</a></li>
          </ul>
        </div>
      </div>
    `
  }

  static get styles() {
    return css`
      .sermon-card {
        display: flex;
        justify-content: center;

        a {
          font-weight: bold;
          text-decoration: none;
          color: var(--theme-primary);
          &:visited {
            color: var(--theme-primary);
          }
        }

        ul {
          list-style-type: none;
          margin: 0;
          padding: 0;
        }
      }
      .sermon-card__inner {
        width: 400px;

        border-radius: 8px;
        border: 2px solid var(--theme-secondary);
      }

      .sermon-card__info {
        padding: 0.5rem;
      }

      .sermon-card__header {
        display: flex;
        justify-content: space-between;
        margin-bottom: 0.5rem;
      }

      .sermon-card__scriptures {
        ul {
          padding-left: 1rem;
          display: inline-flex;
          flex-direction: column;
        }
      }

      .sermon-card__links {
        display: flex;
        justify-content: space-around;

        margin-top: 0.5rem;
        padding: 0.5rem;
        border-top: 2px solid var(--theme-secondary);
      }
    `
  }
}

window.customElements.define("sermon-card", SermonCard)
