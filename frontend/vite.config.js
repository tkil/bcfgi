import { defineConfig } from "vite"
import handlebars from "vite-plugin-handlebars"

import { fileURLToPath } from "url"
import { readdirSync } from "fs"
import path from "path"

const __dirname = path.dirname(fileURLToPath(import.meta.url))

const DIR_PAGES = path.join(__dirname, "src", "pages")

const IS_DEV_MODE = process.env.DEV_MODE === "true"

const getHtmlPagesFromFileSystem = () =>
  readdirSync(DIR_PAGES).reduce((acc, name) => {
    if (/\.html/.test(name)) {
      acc[name.replace(".", "")] = fileURLToPath(
        new URL(path.join(DIR_PAGES, name), import.meta.url)
      )
    }
    return acc
  }, {})

export default defineConfig({
  build: {
    outDir: 'dist/',
    rollupOptions: {
      input: getHtmlPagesFromFileSystem(),
    },
    target: "esnext",
  },
  plugins: [
    handlebars({
      context: {
        basePath: IS_DEV_MODE ? '/src/pages' : "",
        title: "Bible Fellowship Center of Grand Island",
      },
      partialDirectory: path.join(__dirname, 'src', 'partials'),
    }),
    {
      name: "remove-src-pages-dir-from-html-path",
      enforce: "post",
      generateBundle(_, bundle) {
        const htmlFileInSrcFolderPattern = /^src\/pages\/.*\.html$/
        for (const outputItem of Object.values(bundle)) {
          if (!htmlFileInSrcFolderPattern.test(outputItem.fileName)) {
            continue
          }
          outputItem.fileName = outputItem.fileName.replace("src/pages/", "")
        }
      },
    },
  ],
  server: {
    port: 8080,
    open: '/src/pages/index.html',
  },
})
