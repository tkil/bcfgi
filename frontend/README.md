# Bible Fellowship Center Website
#### Repo: [REPO URL]
#### Deploy Job: [DEPLOY URL]

### Summary

### External Dependencies

### Technologies Used
  

[Lit Element](https://lit.dev/)
  Lightweight library for aiding in the development of W3C standard Web Components.

[Vite](https://vitejs.dev/)
  Build tooling

### Prerequisites

### Installation

### Running Project

### Running Tests

### CI / Deployment

### Author
* Dev Author <devauthor@address.com>
